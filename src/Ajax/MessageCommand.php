<?php

declare(strict_types = 1);

namespace Drupal\edit_ui\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Ajax\CommandWithAttachedAssetsTrait;

/**
 * Provides an AJAX command for showing the messages.
 */
class MessageCommand implements CommandInterface {

  use CommandWithAttachedAssetsTrait;

  /**
   * Default animation speed.
   *
   * @var int
   */
  const DEFAULT_SPEED = 500;

  /**
   * The renderable array for messages.
   *
   * @var array
   */
  protected $content = ['#type' => 'status_messages'];

  /**
   * The animation speed.
   *
   * @var int
   */
  protected $speed;

  /**
   * Constructs a MessageCommand object.
   *
   * @param int $speed
   *   The speed of the animation.
   */
  public function __construct($speed = self::DEFAULT_SPEED) {
    $this->speed = $speed;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'editUiAddMessage',
      'content' => $this->getRenderedContent(),
      'speed' => $this->speed,
    ];
  }

}
