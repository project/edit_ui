<?php

declare(strict_types = 1);

namespace Drupal\edit_ui\Ajax;

use Drupal\block\BlockInterface;
use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Ajax\CommandWithAttachedAssetsTrait;
use Drupal\Component\Utility\Html;

/**
 * Provides an AJAX command for showing the messages.
 */
class AddBlockCommand implements CommandInterface {

  use CommandWithAttachedAssetsTrait;

  /**
   * The block entity.
   *
   * @var \Drupal\block\Entity\Block
   */
  protected $block;

  /**
   * The renderable content.
   *
   * @var array
   */
  protected $content = [];

  /**
   * Constructs an InsertCommand object.
   *
   * @param \Drupal\block\BlockInterface $block
   *   The Block.
   */
  public function __construct(BlockInterface $block) {
    $this->block = $block;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $entity_manager = \Drupal::entityTypeManager();

    if ($this->block->access('view')) {
      $this->content = $entity_manager->getViewBuilder($this->block->getEntityTypeId())->view($this->block);
    }
    else {
      $this->content = [];
    }

    $plugin_definition = $this->block->getPlugin()->getPluginDefinition();
    return [
      'command'   => 'editUiAddNewBlock',
      'id'        => $this->block->getOriginalId(),
      'plugin_id' => $this->block->getPluginId(),
      'region'    => $this->block->getRegion(),
      'weight'    => $this->block->getWeight(),
      'label'     => $this->block->label(),
      'status'    => $this->block->status(),
      'html_id'   => Html::getId('block-' . $this->block->getOriginalId()),
      'provider'  => $plugin_definition['provider'],
      'content'   => $this->getRenderedContent(),
    ];
  }

}
