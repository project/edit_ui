<?php

declare(strict_types = 1);

namespace Drupal\edit_ui_block\Plugin\DisplayVariant;

use Drupal\block\Plugin\DisplayVariant\BlockPageVariant;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A page display variant that adds decorations for edit_ui_block interface.
 *
 * @PageDisplayVariant(
 *   id = "edit_ui_block_page",
 *   admin_label = @Translation("Page with all regions and blocks")
 * )
 */
class EditUiBlockPageVariant extends BlockPageVariant {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = parent::build();
    $theme = $this->configFactory->get('system.theme')->get('default');
    $visible_regions = system_region_list($theme, REGIONS_VISIBLE);

    foreach (array_keys($visible_regions) as $region) {
      if (empty($build[$region])) {
        $build[$region] = [];
      }

      // Add edit_ui_block region block at the beginning of each visible
      // regions.
      $edit_ui_block_region_block = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'edit-ui__region-block',
            'js-edit-ui__region-block',
          ],
        ],
        'label' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $visible_regions[$region],
          '#attributes' => [
            'class' => [
              'edit-ui__region-label',
            ],
          ],
        ],
      ];
      array_unshift($build[$region], $edit_ui_block_region_block);

      // Add edit_ui_block filling block at the end of each visible regions.
      $edit_ui_block_filling_block = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => [
            'edit-ui__region-placeholder',
          ],
        ],
      ];
      array_push($build[$region], $edit_ui_block_filling_block);
    }

    return $build;
  }

}
