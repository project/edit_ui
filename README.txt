CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module intends to improve the Drupal administration interface by adding
more administration features directly on frontend pages.

Currently, this module improves block management by adding a new toolbar in the
administration menu available in frontend pages.

The Edit UI module does nothing if enabled alone.

You will need to activate one or more of its sub-modules to get some benefits:
 * edit_ui_block: Allows the block layout to be managed directly in the frontend
   with drag and drop.
 * edit_ui_contextual: Improves contextual action for blocks.

Available features:
 * Block creation directly from frontend pages.
 * Block AJAX deletion using the contextual menu.
 * Blocks and regions highlighting to better visualize the page's layout.
 * Block region management with drag and drop (including the "disabled" region).
 * Block weight management inside a region with drag and drop.
 * Block AJAX deletion using using drag an drop into the trash.
 * Choose between an immediate server syncing or a button that will save your
   layout only when you are done with it.


REQUIREMENTS
------------

This module requires the following modules:
 * Block (Core)


INSTALLATION
------------

 * Install and enable this module like any other drupal 8 module.


CONFIGURATION
-------------

 * Enable the Edit UI block module on your site.
 * Go to the configuration page (/admin/config/user-interface/edit-ui-block)
   and choose your module settings.


MAINTAINERS
-----------

Current maintainers:
 * Tony Cabaye (tocab) - https://www.drupal.org/user/886920

This project has been sponsored by:
 * Smile - http://www.smile.fr
   Sponsored initial development, maintenance and support.
